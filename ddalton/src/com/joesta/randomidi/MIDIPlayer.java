package com.joesta.randomidi;

import javax.sound.midi.*;

/**
 * An object for MIDI. Who woulda thought?
 */
public class MIDIPlayer {

    public MIDIPlayer() {
        // yeah constructors yay
    }

    public void playMusic(ShortMessage[] music) {
        Receiver r;
        try {
            r = MidiSystem.getReceiver();

            for (ShortMessage s : music) {
                r.send(s, -1);
            }

            // the jargon up there plays the byte array... seriously!
        } catch (MidiUnavailableException e) {
            Main.error("Ooops... I guess MIDI is unavailable.");
        } catch (Exception ex) {
            Main.error(ex);
        }
    }

}
