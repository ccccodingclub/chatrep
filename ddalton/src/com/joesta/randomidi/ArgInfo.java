package com.joesta.randomidi;

import java.util.Random;

/**
 * ARGGGGG!
 * <p>
 * In all reality, this is just an object to store
 * input data from the user. Who knows, this could
 * come in handy! :)
 */
public class ArgInfo {

    /**
     * The amount of bytes to generate.
     */
    public int notes = new Random().nextInt(100000000);

    /**
     * Does the user want to output it to a file?
     */
    public boolean outputToFile = false;

    /**
     * The file name the user wants.
     */
    public String fileName = null;

    public ArgInfo() {

    }

    /**
     * A simpler way to parse arguments
     *
     * @param args Argh!!!
     */
    public void parseArgs(String[] args) {
        boolean listenForNotes = false;
        boolean listenForFilename = false;

        for (int c = 0; c < args.length; c++) {
            // this loop will never be executed if we have a null array

            // first up: check to see if we are listening for bytes or
            // if we're listening for a filename.
            if (listenForNotes) {
                // set bytes equal to user input
                try {
                    notes = Integer.parseInt(args[c]);
                    if (notes < 1) {
                        // the user shouldn't be allowed to enter a negative number.
                        throw new Exception("negative number entered");
                    }

                    // stop listening for notes
                    listenForNotes = false;
                } catch (Exception e) {
                    Main.error("Invalid number entered... you entered " + args[c] + ".");
                }
            } else if (listenForFilename) {
                // listen for a filename
                fileName = args[c];
                outputToFile = true;

                // stop listening for filename
                listenForFilename = false;
            } else {
                switch (args[c]) {
                    case "-n": {
                        // listen for bytes
                        listenForNotes = true;
                        break;
                    }
                    case "-r": {
                        // listen for a return filename
                        listenForFilename = true;
                        break;
                    }
                    case "-h": {
                        /*
                        Listen for help...
                        the user needs help...
                        help... help us...
                         */
                        Main.help();
                        break;
                    }
                }
            }
        }
    }

}
