package com.joesta.randomidi;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.ShortMessage;
import java.util.Random;

/**
 * A class to generate bytes.
 * We don't want one function call to do this.
 */
public class ByteGenerator {

    public ByteGenerator() {
        // constructor constructs here
    }

    public ShortMessage[] genRandomBytes(int amountOfBytes) {
        Random randy = new Random();
        ShortMessage[] outBytes = new ShortMessage[amountOfBytes];

        // this is actually the code behind Random.nextBytes(outBytes).
        // with some modification, so it fits the MIDI standard, of course :)
        // see the MIDI specification
        // http://www.midi.org/techspecs/midimessages.php
        for (int c = 0; c < amountOfBytes; c++) {
            ShortMessage s = new ShortMessage();
            try {
                // makes a randomized command byte, a randomized channel, and two randomized data bytes.
                // Java has signed bytes.
                if (randy.nextInt(769) > 767) {
                    int i, j, k, l;
                    i = randy.nextInt(128);
                    j = randy.nextInt(128);
                    k = giveCommandByte();
                    l = randy.nextInt(16);
                    System.out.println("i: " + i + "; j: " + j + "; k: " + k + "; l: " + l);
                    s = new ShortMessage(k, l, i, j);
                } else {
                    s = new ShortMessage(ShortMessage.TIMING_CLOCK);
                    // someone fix this part so it times better
                }
            } catch (InvalidMidiDataException e) {
                Main.error(e);
            }
            outBytes[c] = s;
        }

        return outBytes;
    }

    public byte[] changeToByteArray(ShortMessage[] s) {
        byte[] b = new byte[s.length * 4];
        int i = 0;
        for (int c = 0; c < s.length * 4; c+=4) {
            b[c] = (byte) s[i].getCommand();
            b[c+1] = (byte) s[i].getChannel();
            b[c+2] = (byte) s[i].getData1();
            b[c+3] = (byte) s[i].getData2();
            i++;
        }
        return b;
    }

    public int giveCommandByte() {
        Random randy = new Random();
        int i;
        switch (randy.nextInt(7)) {
            case 0: {
                i = ShortMessage.CHANNEL_PRESSURE;
                break;
            }
            case 1: {
                i = ShortMessage.CONTROL_CHANGE;
                break;
            }
            case 2: {
                i = ShortMessage.NOTE_OFF;
                break;
            }
            case 3: {
                i = ShortMessage.NOTE_ON;
                break;
            }
            case 4: {
                i = ShortMessage.PITCH_BEND;
                break;
            }
            case 5: {
                i = ShortMessage.POLY_PRESSURE;
                break;
            }
            default: {
                i = ShortMessage.PROGRAM_CHANGE;
                break;
            }
        }
        return i;
    }
}
