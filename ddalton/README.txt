Hello, and welcome to RandoMIDI!

Yes, this program actually does have ways to run it, although, it is randomized.

It is built/compiled/edited with IntelliJ IDEA. I like being simple.

Syntax of running it:

	java -jar randomidi.jar (-n notes) (-h) (-r filename)

	where...

	-n	- amount of notes to generate
	-h	- displays a nice, user-friendly help menu
	-r	- returns (writes) a MIDI file (with supplied filename)

There is structure to randomness, as there is randomness in structure.

About the sounds/ directory…

	- I wanted a MIDI to read hex data from, so I could attempt to understand
	  MIDI Structure.
	- There are some randomized test MIDI’s, but these are just randomized bytes.
	  These will not help anyone understand anything. You can, however, import
	  the raw audio bytes into Audacity and hear some static. Play with it. You
	  might find a poltergeist…


