import java.net.*;
import java.io.*;

class Chat
{
	void createSocket( String hostname, int portNum)
	{
		/////////////////////////////////////////
		// Creates a socket, which establishes connection to a SERVER. 
 
		System.out.println("HOSNAME IS "+hostname+" AND PORT NUMBER IS "+portNum);
		try 
		(
			/////////////////////////
			// Attempting to create and establish connection to  a server
			// using a socket.
			Socket mySocket  = new Socket(hostname, portNum);
			PrintWriter out  = new PrintWriter(mySocket.getOutputStream(),true);
			BufferedReader in = new BufferedReader(
									new InputStreamReader(mySocket.getInputStream()));
			BufferedReader stdIn = new BufferedReader(
									new InputStreamReader(System.in));	
		) {	
		
			String userInput;
			String readInput;
			boolean forever = true;
			//while ((userInput = stdIn.readLine()) != null)
			while(forever == true)
			{
				System.out.println("RECIEVING JUNK");
				////////////////////////////////////////////
				// sending the TYPED input to the server
				while((userInput = stdIn.readLine()) != null)
				{
					out.println(userInput);		
					out.flush();
				}
				while((readInput = in.readLine()) != null)
				{
					/////////////////////////
					// only printing when receving so don't have to wait 	
					System.out.println("server: "+readInput);
				}
			}
			System.out.println("forever iz: "+forever);
		}
		catch( UnknownHostException e)
		{
			System.err.println("Don't know about host "+hostname);
			System.exit(0);
		}
		catch( IOException e)
		{
			System.err.println("Couldn't get I/O for the connection to " +hostname);
			System.exit(0);
		}
		
	}
	void createServer( int portNum)
	{
		System.out.println("LISTENING ON PORT "+portNum);
		try (
			ServerSocket serverSocket = new ServerSocket(portNum);
			Socket clientSocket = serverSocket.accept();
			PrintWriter out =
				new PrintWriter( clientSocket.getOutputStream(), true);
			BufferedReader in = 
				new BufferedReader( new InputStreamReader( 
									clientSocket.getInputStream()));
				BufferedReader stdIn = new BufferedReader(
									new InputStreamReader(System.in));	
			)
			{
				String inputLine;
				String sendLine;
				//while(( inputLine = in.readLine()) != null)
				while (true)
				{
					while((inputLine = in.readLine()) != null)
					{
						////////////////////////////////
						// There's a recieved message to print!
						System.out.println("client: "+inputLine);
					}
					while((sendLine = stdIn.readLine()) != null)
					{
						out.println(sendLine);
						out.flush();
					}
				}
			}
			catch (IOException e) 
			{
				System.out.println("Exception caught when trying to listen on port");
				System.out.println(e.getMessage());
			}
	}
			
	public static void main(String args[])
	{
		if ( args.length < 1)
		{
			System.out.println("Error: must construct additional arguments!");
			System.exit(0);
		}
		Chat instance = new Chat();
		
		if ( args[0].charAt(0) == 'S' || args[0].charAt(0) == 's')
			instance.createServer(Integer.parseInt(args[1]));
		else if ( args[0].charAt(0) == 'C' || args[0].charAt(0) == 'c')
			instance.createSocket(args[1], Integer.parseInt(args[2]));
	}
}
