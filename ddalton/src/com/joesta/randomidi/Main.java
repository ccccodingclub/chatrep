package com.joesta.randomidi;

import javax.sound.midi.ShortMessage;
import java.io.File;

/**
 * Simply a Main class for the application to run off of.
 * It's easier to have this class structurally execute instructions
 * in the order of the program, and let each other sub-class
 * do its own thing.
 */
public class Main {

    /**
     * An argument holder.
     */
    private static ArgInfo argh = new ArgInfo();

    /**
     * A byte generator.
     */
    private static ByteGenerator bg = new ByteGenerator();

    /**
     * A player for the MIDI file.
     */
    private static MIDIPlayer mp = new MIDIPlayer();

    /**
     * The illustrious main() method that kicks everything off.
     *
     * @param args a String[] array of arguments for the program to parse and handle.
     */
    public static void main(String[] args) {
        // first off, before we do anything, did the user run this thing with any arguments?
        // that would be good to know...

        System.out.println("Reading in what you want me to do...");

        argh.parseArgs(args); // argh!!!

        // now that we know what the user wants, we can try to give that to them.

        System.out.println("Generating " + argh.notes + " notes...");

        ShortMessage[] midi = bg.genRandomBytes(argh.notes);
        byte[] b = bg.changeToByteArray(midi);

        // with the new byte[] array, simply load it in to the synthesiser

        System.out.println("Playing music out of this byte array...");

        mp.playMusic(midi);

        if (argh.outputToFile) {
            System.out.println("Saving file to " + argh.fileName + "...");
            Filesaver f = new Filesaver();
            f.save(argh.fileName, b);
            System.out.println("File I/O done.");
        }
    }

    /**
     * An error occurred that **may** have been the user's fault.
     *
     * @param message A string to display to the user.
     */
    public static void error(String message) {
        System.out.println("Error: " + message + "\n");
        System.exit(0);
    }

    /**
     * Only run if there was an exception that wasn't just user error.
     *
     * @param ex A Java exception. We can print the stack trace.
     */
    public static void error(Exception ex) {
        System.out.println("Usage\n" +
                "\n" +
                "\tjava -jar randomidi.jar (-n notes) (-h) (-r filename)\n" +
                "\n" +
                "\twhere...\n" +
                "\n" +
                "\t-n\t- amount of notes to generate\n" +
                "\t-h\t- displays a nice, user-friendly help menu\n" +
                "\t-r\t- returns (writes) a MIDI file (with supplied filename)");
        ex.printStackTrace();
        System.exit(0);
    }

    /**
     * A nice, user-friendly help dialog
     */
    public static void help() {
        System.out.println("Usage\n" +
                "\n" +
                "\tjava -jar randomidi.jar (-n notes) (-h) (-r filename)\n" +
                "\n" +
                "\twhere...\n" +
                "\n" +
                "\t-n\t- amount of notes to generate\n" +
                "\t-h\t- displays a nice, user-friendly help menu\n" +
                "\t-r\t- returns (writes) a MIDI file (with supplied filename)");
        System.exit(0);
    }
}
