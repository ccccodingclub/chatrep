package com.joesta.randomidi;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * A class to save files.
 */
public class Filesaver {
    public Filesaver() {

    }

    /**
     * Save the file.
     *
     * @param fileName the name of the file to save.
     * @param data data to write
     */
    public void save(String fileName, byte[] data) {
        try {
            // prepare for I/O
            FileOutputStream f = new FileOutputStream(fileName);

            // write the file out
            f.write(data);

            // close the stream
            f.close();
        } catch (IOException e) {
            Main.error("Could not make/save to file " + fileName + ".");
        } catch (Exception e) {
            Main.error(e);
        } finally {
            // do nothing
        }
    }
}
